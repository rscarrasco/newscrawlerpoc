# -*- coding: utf-8 -*-
import datetime
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, Compose
from newscrawlerpoc.items import Article


class TheguardianttSpider(CrawlSpider):
    name = 'theguardiantt'
    allowed_domains = ['www.guardian.co.tt']
    start_urls = ['http://www.guardian.co.tt/']

    rules = (
        Rule(LinkExtractor(restrict_xpaths='//a[./div[text()="News"]]'), follow=True),
        Rule(LinkExtractor(restrict_xpaths="//div[@class=' aghor agjcfs agaifs']//span[@class='ag_page_navigation_top']/a[text()='› ']"), follow=True),
        Rule(LinkExtractor(restrict_xpaths='//div[@class=" aghor agjcfs agaifs"]//a[.//h1]'), callback='parse_article', follow=True),
    )

    def parse_article(self, response):
        loader = ItemLoader(item = Article(), response = response)
        loader.default_output_processor = TakeFirst()
        loader.pubDate_out = Compose(TakeFirst(), lambda a : datetime.datetime.strptime(a, '%Y%m%d').isoformat())
        loader.contentsHTML_out = Compose(TakeFirst(), lambda a : a.replace('\xad', ''))
        loader.add_xpath('title', '//div[@class=" aghor agjcfs agaifs article-wrapper ag-main-col"]/div[@class=" agver agjcfs agaifs"]//h1/text()')
        loader.add_xpath('pubDate', '//span[@class="textelement-publishing date"]/text()')
        loader.add_xpath('author', '//span[@class="text-editor string"]/text()')
        loader.add_css('contentsHTML', 'div.article-wrapper')
        loader.add_value('sourceURL', response.url)
        loader.add_value('crawlDate', str(datetime.datetime.now().isoformat()))
        loader.add_value('lang', 'en')
        return loader.load_item()
