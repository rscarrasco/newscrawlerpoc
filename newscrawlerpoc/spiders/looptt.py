# -*- coding: utf-8 -*-
import datetime
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, Compose
from newscrawlerpoc.items import Article


class LoopttSpider(CrawlSpider):
    name = 'looptt'
    allowed_domains = ['looptt.com']
    start_urls = ['https://www.looptt.com/category/looptt-environment']

    rules = (
        Rule(LinkExtractor(restrict_css='li.pager__item--next'), follow=True),
        Rule(LinkExtractor(restrict_css='div.view-content a'), callback='parse_article', follow=True),
    )

    def parse_article(self, response):
        loader = ItemLoader(item = Article(), response = response)
        loader.default_output_processor = TakeFirst()
        loader.default_input_processor = Compose(TakeFirst(), str.strip)
        loader.pubDate_out = Compose(TakeFirst(), lambda a : datetime.datetime.strptime(a[10:], '%d %B %Y').isoformat())
        loader.add_css('title', 'article h2 span::text')
        loader.add_css('pubDate', 'article div.author-info span.date-tp-4::text')
        loader.add_css('author', 'article span.author-name::text')
        loader.add_css('contentsHTML', 'article.node--type-article')
        loader.add_value('sourceURL', response.url)
        loader.add_value('crawlDate', str(datetime.datetime.now().isoformat()))
        loader.add_value('lang', 'en')
        return loader.load_item()
