# -*- coding: utf-8 -*-
import scrapy
import logging
import datetime
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, Join, Compose
from newscrawlerpoc.items import Article


class TheguardianSpider(CrawlSpider):
    name = 'theguardian'
    allowed_domains = ['www.theguardian.com']
    start_urls = ['https://www.theguardian.com/environment?page=1']

    rules = (
        Rule(LinkExtractor(deny='\/all$', restrict_css='.pagination__action--static'), follow=True),
        Rule(LinkExtractor(deny='\/video\/', restrict_css='a.js-headline-text'), callback='parse_article', follow=True)
    )

    def parse_article(self, response):
        loader = ItemLoader(item = Article(), response = response)
        loader.default_output_processor = Join()
        loader.default_input_processor = Compose(TakeFirst(), str.strip)
        loader.add_css('title', 'h1.content__headline::text')
        loader.add_css('pubDate', 'time.content__dateline-wpd::attr(datetime)')
        loader.add_css('author', 'div.meta__contact-wrap span span::text')
        loader.add_css('author', 'h1.content__headline + span a span::text')
        loader.add_css('contentsHTML', 'div.content__article-body')
        loader.add_value('sourceURL', response.url)
        loader.add_value('crawlDate', str(datetime.datetime.now().isoformat()))
        loader.add_value('lang', 'en')
        return loader.load_item()
