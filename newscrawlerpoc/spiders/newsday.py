# -*- coding: utf-8 -*-
import datetime
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, Compose
from newscrawlerpoc.items import Article


class NewsdaySpider(CrawlSpider):
    name = 'newsday'
    allowed_domains = ['newsday.co.tt']
    start_urls = ['https://newsday.co.tt/category/news/']

    rules = (
        Rule(LinkExtractor(restrict_css='a.next'), follow=True),
        Rule(LinkExtractor(restrict_css='div.article-text-block h3 a'), callback='parse_article', follow=True),
    )

    def parse_article(self, response):
        loader = ItemLoader(item = Article(), response = response)
        loader.default_output_processor = TakeFirst()
        loader.default_input_processor = Compose(TakeFirst(), str.strip)
        loader.add_css('title', 'h1::text')
        loader.add_css('pubDate', 'time.date-time::attr(datetime)')
        loader.add_css('author', 'address.article-author a::text')
        loader.add_css('contentsHTML', 'article.article-content')
        loader.add_value('sourceURL', response.url)
        loader.add_value('crawlDate', str(datetime.datetime.now().isoformat()))
        loader.add_value('lang', 'en')
        return loader.load_item()
