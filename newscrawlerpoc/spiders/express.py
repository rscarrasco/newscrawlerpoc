# -*- coding: utf-8 -*-
import datetime
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, Compose
from newscrawlerpoc.items import Article


class ExpressSpider(CrawlSpider):
    name = 'express'
    allowed_domains = ['www.express.co.uk']
    start_urls = ['https://www.express.co.uk/sitearchive']

    rules = (
        Rule(LinkExtractor(allow='\/sitearchive\/', restrict_css='section.sitemap a'), follow=True),
        Rule(LinkExtractor(restrict_css='section.sitemap a'), callback='parse_article', follow=True),
    )

    def parse_article(self, response):
        loader = ItemLoader(item = Article(), response = response)
        loader.default_output_processor = TakeFirst()
        loader.default_input_processor = Compose(TakeFirst(), str.strip)
        loader.add_css('title', 'h1::text')
        loader.add_css('pubDate', 'div.publish-info time::attr(datetime)')
        loader.add_css('author', 'div.author span::text')
        loader.add_css('contentsHTML', 'article')
        loader.add_value('sourceURL', response.url)
        loader.add_value('crawlDate', str(datetime.datetime.now().isoformat()))
        loader.add_value('lang', 'en')
        return loader.load_item()
